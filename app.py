import xmlrpc.client
import requests
from datetime import datetime, timedelta

# Fonction pour obtenir les détails de l'utilisateur et les données TimeCamp
def get_user_details(api_token):
    url_user = "https://app.timecamp.com/third_party/api/user?format=json"
    url_time = "https://app.timecamp.com/third_party/api/entries"

    querystring = {"from":"2024-03-01","to":"2024-05-30"}

    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + api_token
    }
    

    response = requests.get(url_user, headers=headers)
    data1 = response.json()

    # Extraire le display_name (test)
    display_name = data1.get('display_name')
    print("Nom d'utilisateur:", display_name)

    response_time = requests.get(url_time, headers=headers, params=querystring)
    data_time = response_time.json()

    return data_time


# Paramètres de connexion Odoo
url = 'http://localhost:8069'  
db = 'SIE_Nico_DB'
username = 'sie@gmail.com'
password = '12345'

# Connexion à Odoo
common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})

models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

class AttendanceUpdate:
    @staticmethod
    def import_work_hours_from_timecamp(api_token):
        data_timecamp = get_user_details(api_token)
        success_count = 0
        failure_count = 0

        for entry in data_timecamp:
            # Convertir les timestamps en objets datetime
            start_time = datetime.strptime(entry['start_time'], '%H:%M:%S')
            end_time = datetime.strptime(entry['end_time'], '%H:%M:%S')
            
             # Ajouter un décalage de deux heures pour correspondre à Odoo
            start_time -= timedelta(hours=2)
            end_time -= timedelta(hours=2)

            # Formater la date dans le format attendu par Odoo (année-mois-jour)
            entry_date = entry['date']
            
            # Concaténer la date avec l'heure dans le format approprié pour Odoo
            check_in = entry_date + ' ' + start_time.strftime('%H:%M:%S')
            check_out = entry_date + ' ' + end_time.strftime('%H:%M:%S')
            print(check_in)

             # Vérifier si une entrée similaire existe déjà dans Odoo
            existing_attendance = models.execute_kw(db, uid, password, 'hr.attendance', 'search_count', 
                                                    [[['employee_id', '=', 21], 
                                                    ['check_in', '=', check_in]]])


            if existing_attendance == 0:
                # Calculer la durée de travail en heures
                duration_hours = (end_time - start_time).total_seconds() / 3600

                # Créer un enregistrement d'heure de travail dans Odoo Attendance
                attendance_data = {
                    'employee_id': 21,  # ID de l'employé 
                    'check_in': check_in,
                    'check_out': check_out,
                    'worked_hours': duration_hours,
                }

                try:
                    attendance_id = models.execute_kw(db, uid, password, 'hr.attendance', 'create', [attendance_data])
                    print("Entrée ajouté avec succès.")
                    success_count += 1
                except xmlrpc.client.Fault as error:
                    print("Échec de l'ajout de l'entrée:", error)
                    failure_count += 1

            else:
                print("Cette entrée existe déjà")


        print("Nombre d'enregistrements d'heure de travail ajoutés avec succès:", success_count)
        print("Nombre d'échecs d'ajout d'enregistrements d'heure de travail:", failure_count)

# Appel de la méthode pour importer les heures de travail depuis TimeCamp vers Odoo Attendance
api_token = "6ce5868edd4aee16250422bd29"  # Remplacez par votre token TimeCamp
AttendanceUpdate.import_work_hours_from_timecamp(api_token)
